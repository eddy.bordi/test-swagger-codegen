# coding: utf-8

"""
    Prevision.io

    Prevision.io Studio APIs  # noqa: E501

    OpenAPI spec version: 1.0.5
    Contact: web@prevision.io
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.data_source_api import DataSourceApi  # noqa: E501
from swagger_client.rest import ApiException


class TestDataSourceApi(unittest.TestCase):
    """DataSourceApi unit test stubs"""

    def setUp(self):
        self.api = DataSourceApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_data_source_connection_test(self):
        """Test case for data_source_connection_test

        Data source connection test  # noqa: E501
        """
        pass

    def test_get_data_source(self):
        """Test case for get_data_source

        Get data-source  # noqa: E501
        """
        pass

    def test_new_data_source_connection_test(self):
        """Test case for new_data_source_connection_test

        New data source connection test  # noqa: E501
        """
        pass

    def test_remove_data_source(self):
        """Test case for remove_data_source

        Remove data-source  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
