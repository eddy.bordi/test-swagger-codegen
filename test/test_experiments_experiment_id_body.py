# coding: utf-8

"""
    Prevision.io

    Prevision.io Studio APIs  # noqa: E501

    OpenAPI spec version: 1.0.5
    Contact: web@prevision.io
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.models.experiments_experiment_id_body import ExperimentsExperimentIdBody  # noqa: E501
from swagger_client.rest import ApiException


class TestExperimentsExperimentIdBody(unittest.TestCase):
    """ExperimentsExperimentIdBody unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testExperimentsExperimentIdBody(self):
        """Test ExperimentsExperimentIdBody"""
        # FIXME: construct object with mandatory attributes with example values
        # model = swagger_client.models.experiments_experiment_id_body.ExperimentsExperimentIdBody()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
