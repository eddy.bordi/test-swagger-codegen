# coding: utf-8

"""
    Prevision.io

    Prevision.io Studio APIs  # noqa: E501

    OpenAPI spec version: 1.0.5
    Contact: web@prevision.io
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.deployment_prediction_api import DeploymentPredictionApi  # noqa: E501
from swagger_client.rest import ApiException


class TestDeploymentPredictionApi(unittest.TestCase):
    """DeploymentPredictionApi unit test stubs"""

    def setUp(self):
        self.api = DeploymentPredictionApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_download_prediction(self):
        """Test case for download_prediction

        download prediction  # noqa: E501
        """
        pass

    def test_get_prediction_by_id(self):
        """Test case for get_prediction_by_id

        get prediction  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
