# DatasetsFileBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **str** | file to upload | [optional] 
**name** | **str** | dataset name | [optional] 
**separator** | **str** | dataset separator | [optional] 
**decimal** | **str** | dataset char | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

