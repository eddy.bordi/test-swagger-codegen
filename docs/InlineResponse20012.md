# InlineResponse20012

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**list[DefinitionsdatasetColumn]**](DefinitionsdatasetColumn.md) |  | [optional] 
**meta_data** | [**DefinitionsmetaData**](DefinitionsmetaData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

