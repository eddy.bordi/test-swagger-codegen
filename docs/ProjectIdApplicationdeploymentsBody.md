# ProjectIdApplicationdeploymentsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**git_url** | **str** |  | [optional] 
**git_branch** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**broker** | **str** |  | [optional] 
**app_cpu** | **float** |  | [optional] 
**app_ram** | **str** |  | [optional] 
**app_replica_count** | **float** |  | [optional] 
**env_vars** | **str** |  | [optional] 
**access_type** | **str** |  | [optional] 
**description** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

