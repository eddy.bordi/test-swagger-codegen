# swagger_client.DatasetApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_dataset**](DatasetApi.md#delete_dataset) | **DELETE** /datasets/{datasetId} | Remove dataset
[**download_dataset**](DatasetApi.md#download_dataset) | **GET** /datasets/{datasetId}/download | Download dataset
[**get_dataset_by_id**](DatasetApi.md#get_dataset_by_id) | **GET** /datasets/{datasetId} | Find dataset by ID
[**get_dataset_columns**](DatasetApi.md#get_dataset_columns) | **GET** /datasets/{datasetId}/columns | list dataset&#x27;s columns
[**get_dataset_correlation_mx**](DatasetApi.md#get_dataset_correlation_mx) | **GET** /datasets/{datasetId}/correlation-matrix | get dataset&#x27;s correlation-matrix
[**get_dataset_sample**](DatasetApi.md#get_dataset_sample) | **GET** /datasets/{datasetId}/sample | get dataset sample
[**launch_analysis**](DatasetApi.md#launch_analysis) | **POST** /datasets/{datasetId}/analysis | Launch dataset analysis
[**launch_parse**](DatasetApi.md#launch_parse) | **PUT** /datasets/{datasetId}/parse | Launch dataset parse

# **delete_dataset**
> delete_dataset()

Remove dataset

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DatasetApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of dataset to delete

try:
    # Remove dataset
    api_instance.delete_dataset()
except ApiException as e:
    print("Exception when calling DatasetApi->delete_dataset: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of dataset to delete | 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **download_dataset**
> download_dataset()

Download dataset

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DatasetApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of dataset to download

try:
    # Download dataset
    api_instance.download_dataset()
except ApiException as e:
    print("Exception when calling DatasetApi->download_dataset: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of dataset to download | 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_dataset_by_id**
> Definitionsdataset get_dataset_by_id()

Find dataset by ID

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DatasetApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of dataset to return

try:
    # Find dataset by ID
    api_response = api_instance.get_dataset_by_id()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DatasetApi->get_dataset_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of dataset to return | 

### Return type

[**Definitionsdataset**](Definitionsdataset.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_dataset_columns**
> InlineResponse20012 get_dataset_columns()

list dataset's columns

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DatasetApi()
 = swagger_client.null() #  | ID of dataset to return

try:
    # list dataset's columns
    api_response = api_instance.get_dataset_columns()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DatasetApi->get_dataset_columns: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of dataset to return | 

### Return type

[**InlineResponse20012**](InlineResponse20012.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_dataset_correlation_mx**
> DefinitionscorrelationMatrix get_dataset_correlation_mx()

get dataset's correlation-matrix

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DatasetApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of dataset to return

try:
    # get dataset's correlation-matrix
    api_response = api_instance.get_dataset_correlation_mx()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DatasetApi->get_dataset_correlation_mx: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of dataset to return | 

### Return type

[**DefinitionscorrelationMatrix**](DefinitionscorrelationMatrix.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_dataset_sample**
> DefinitionsdatasetSample get_dataset_sample()

get dataset sample

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DatasetApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of dataset to return

try:
    # get dataset sample
    api_response = api_instance.get_dataset_sample()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DatasetApi->get_dataset_sample: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of dataset to return | 

### Return type

[**DefinitionsdatasetSample**](DefinitionsdatasetSample.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **launch_analysis**
> Definitionsdataset launch_analysis()

Launch dataset analysis

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DatasetApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of dataset to analysis

try:
    # Launch dataset analysis
    api_response = api_instance.launch_analysis()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DatasetApi->launch_analysis: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of dataset to analysis | 

### Return type

[**Definitionsdataset**](Definitionsdataset.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **launch_parse**
> Definitionsdataset launch_parse()

Launch dataset parse

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DatasetApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of dataset to parse

try:
    # Launch dataset parse
    api_response = api_instance.launch_parse()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DatasetApi->launch_parse: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of dataset to parse | 

### Return type

[**Definitionsdataset**](Definitionsdataset.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

