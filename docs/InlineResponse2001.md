# InlineResponse2001

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta_data** | [**DefinitionsmetaData**](DefinitionsmetaData.md) |  | [optional] 
**items** | [**list[Definitionsproject]**](Definitionsproject.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

