# ConnectorsConnectorIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **str** | connector username | [optional] 
**password** | **str** | connector password | [optional] 
**port** | **str** | connector port | [optional] 
**host** | **str** | connector host | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

