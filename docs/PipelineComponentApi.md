# swagger_client.PipelineComponentApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_pipeline_component**](PipelineComponentApi.md#delete_pipeline_component) | **DELETE** /pipeline-components/{pipelineComponentId} | delete pipeline-components
[**get_pipeline_component**](PipelineComponentApi.md#get_pipeline_component) | **GET** /pipeline-components/{pipelineComponentId} | get pipeline-components

# **delete_pipeline_component**
> delete_pipeline_component()

delete pipeline-components

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PipelineComponentApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of pipelineComponent to return

try:
    # delete pipeline-components
    api_instance.delete_pipeline_component()
except ApiException as e:
    print("Exception when calling PipelineComponentApi->delete_pipeline_component: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of pipelineComponent to return | 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pipeline_component**
> DefinitionspipelineComponent get_pipeline_component()

get pipeline-components

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PipelineComponentApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of pipelineComponent to return

try:
    # get pipeline-components
    api_response = api_instance.get_pipeline_component()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineComponentApi->get_pipeline_component: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of pipelineComponent to return | 

### Return type

[**DefinitionspipelineComponent**](DefinitionspipelineComponent.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

