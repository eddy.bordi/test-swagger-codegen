# ProjectIdModeldeploymentsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**experiment_id** | **str** |  | [optional] 
**main_model_experiment_version_id** | **str** |  | [optional] 
**main_model_id** | **str** |  | [optional] 
**challenger_model_experiment_version_id** | **str** |  | [optional] 
**challenger_model_id** | **str** |  | [optional] 
**access_type** | **str** |  | [optional] 
**description** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

