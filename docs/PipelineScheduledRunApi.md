# swagger_client.PipelineScheduledRunApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_pipeline**](PipelineScheduledRunApi.md#delete_pipeline) | **DELETE** /pipeline-scheduled-runs/{pipelineScheduledRunId} | delete pipeline-runs
[**disable_pipeline_scheduled_run**](PipelineScheduledRunApi.md#disable_pipeline_scheduled_run) | **POST** /pipeline-scheduled-runs/{pipelineScheduleRunId}/disable | disable pipeline-run
[**enable_pipeline_scheduled_run**](PipelineScheduledRunApi.md#enable_pipeline_scheduled_run) | **POST** /pipeline-scheduled-runs/{pipelineScheduleRunId}/enable | enable pipeline-run
[**get_pipeline_schedule_run**](PipelineScheduledRunApi.md#get_pipeline_schedule_run) | **GET** /pipeline-scheduled-runs/{pipelineScheduledRunId} | get pipeline-run
[**get_pipeline_schedule_runs**](PipelineScheduledRunApi.md#get_pipeline_schedule_runs) | **GET** /pipeline-scheduled-runs/{pipelineScheduledRunId}/runs | get pipeline runs

# **delete_pipeline**
> delete_pipeline()

delete pipeline-runs

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PipelineScheduledRunApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of pipeline-run to delete

try:
    # delete pipeline-runs
    api_instance.delete_pipeline()
except ApiException as e:
    print("Exception when calling PipelineScheduledRunApi->delete_pipeline: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of pipeline-run to delete | 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **disable_pipeline_scheduled_run**
> DefinitionspipelineRun disable_pipeline_scheduled_run()

disable pipeline-run

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PipelineScheduledRunApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of pipeline-run to return

try:
    # disable pipeline-run
    api_response = api_instance.disable_pipeline_scheduled_run()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineScheduledRunApi->disable_pipeline_scheduled_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of pipeline-run to return | 

### Return type

[**DefinitionspipelineRun**](DefinitionspipelineRun.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **enable_pipeline_scheduled_run**
> DefinitionspipelineRun enable_pipeline_scheduled_run()

enable pipeline-run

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PipelineScheduledRunApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of pipeline-run to return

try:
    # enable pipeline-run
    api_response = api_instance.enable_pipeline_scheduled_run()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineScheduledRunApi->enable_pipeline_scheduled_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of pipeline-run to return | 

### Return type

[**DefinitionspipelineRun**](DefinitionspipelineRun.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pipeline_schedule_run**
> DefinitionspipelineRun get_pipeline_schedule_run(, =, =)

get pipeline-run

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PipelineScheduledRunApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of pipeline-run to return
 = swagger_client.null() #  | Number of paginated elements (optional)
 = swagger_client.null() #  | Token to access a specific page, if not provided, the result served will be the first page (optional)

try:
    # get pipeline-run
    api_response = api_instance.get_pipeline_schedule_run(, =, =)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineScheduledRunApi->get_pipeline_schedule_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of pipeline-run to return | 
 **** | [****](.md)| Number of paginated elements | [optional] 
 **** | [****](.md)| Token to access a specific page, if not provided, the result served will be the first page | [optional] 

### Return type

[**DefinitionspipelineRun**](DefinitionspipelineRun.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pipeline_schedule_runs**
> DefinitionspipelineRun get_pipeline_schedule_runs()

get pipeline runs

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PipelineScheduledRunApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of pipeline-run to return

try:
    # get pipeline runs
    api_response = api_instance.get_pipeline_schedule_runs()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineScheduledRunApi->get_pipeline_schedule_runs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of pipeline-run to return | 

### Return type

[**DefinitionspipelineRun**](DefinitionspipelineRun.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

