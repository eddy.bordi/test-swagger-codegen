# UsersUserIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**project_role** | **str** |  | [default to 'admin']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

