# ExperimentversionsexperimentVersionIdunitpredictionFeatures

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**feature1** | **str** |  | [optional] 
**feature2** | **int** |  | [optional] 
**feature3** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

