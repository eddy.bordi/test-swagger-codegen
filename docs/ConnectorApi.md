# swagger_client.ConnectorApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**connector_connection_test**](ConnectorApi.md#connector_connection_test) | **POST** /connectors/{connectorId}/test | Connector connection test
[**connectors_connector_id_databases_database_tables_get**](ConnectorApi.md#connectors_connector_id_databases_database_tables_get) | **GET** /connectors/{connectorId}/databases/{database}/tables | Get connector tables
[**connectors_connector_id_databases_get**](ConnectorApi.md#connectors_connector_id_databases_get) | **GET** /connectors/{connectorId}/databases | Get connector databases
[**connectors_connector_id_get**](ConnectorApi.md#connectors_connector_id_get) | **GET** /connectors/{connectorId} | Get one connector
[**connectors_connector_id_paths_get**](ConnectorApi.md#connectors_connector_id_paths_get) | **GET** /connectors/{connectorId}/paths | Connector files and paths
[**remove_connector**](ConnectorApi.md#remove_connector) | **DELETE** /connectors/{connectorId} | Remove connector

# **connector_connection_test**
> InlineResponse20011 connector_connection_test()

Connector connection test

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ConnectorApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of connector to return

try:
    # Connector connection test
    api_response = api_instance.connector_connection_test()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConnectorApi->connector_connection_test: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of connector to return | 

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **connectors_connector_id_databases_database_tables_get**
> list[str] connectors_connector_id_databases_database_tables_get(, )

Get connector tables

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ConnectorApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of connector to return
 = swagger_client.null() #  | database name

try:
    # Get connector tables
    api_response = api_instance.connectors_connector_id_databases_database_tables_get(, )
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConnectorApi->connectors_connector_id_databases_database_tables_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of connector to return | 
 **** | [****](.md)| database name | 

### Return type

**list[str]**

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **connectors_connector_id_databases_get**
> list[str] connectors_connector_id_databases_get()

Get connector databases

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ConnectorApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of connector to return

try:
    # Get connector databases
    api_response = api_instance.connectors_connector_id_databases_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConnectorApi->connectors_connector_id_databases_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of connector to return | 

### Return type

**list[str]**

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **connectors_connector_id_get**
> Definitionsconnector connectors_connector_id_get(, =, =, =)

Get one connector

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ConnectorApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of connector to return
 = swagger_client.null() #  | select all database tables (optional)
 = swagger_client.null() #  | select all bucket files (optional)
 = swagger_client.null() #  | select all tables (optional)

try:
    # Get one connector
    api_response = api_instance.connectors_connector_id_get(, =, =, =)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConnectorApi->connectors_connector_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of connector to return | 
 **** | [****](.md)| select all database tables | [optional] 
 **** | [****](.md)| select all bucket files | [optional] 
 **** | [****](.md)| select all tables | [optional] 

### Return type

[**Definitionsconnector**](Definitionsconnector.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **connectors_connector_id_paths_get**
> list[str] connectors_connector_id_paths_get()

Connector files and paths

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ConnectorApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of connector to return

try:
    # Connector files and paths
    api_response = api_instance.connectors_connector_id_paths_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConnectorApi->connectors_connector_id_paths_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of connector to return | 

### Return type

**list[str]**

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **remove_connector**
> remove_connector()

Remove connector

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ConnectorApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of connector to delete

try:
    # Remove connector
    api_instance.remove_connector()
except ApiException as e:
    print("Exception when calling ConnectorApi->remove_connector: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of connector to delete | 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

