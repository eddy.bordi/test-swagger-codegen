# swagger_client.DeploymentVersionApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_deployment_drift**](DeploymentVersionApi.md#get_deployment_drift) | **GET** /deployments/{deploymentId}/drift | Get deployment drift
[**get_deployment_usage**](DeploymentVersionApi.md#get_deployment_usage) | **GET** /deployments/{deploymentId}/usage | Get deployment usage
[**roll_back_deployment_version**](DeploymentVersionApi.md#roll_back_deployment_version) | **POST** /deployments/{deploymentId}/versions/{version} | Rollback old version
[**update_description_by_version**](DeploymentVersionApi.md#update_description_by_version) | **PUT** /deployments/{deploymentId}/versions/{version} | Update description by version

# **get_deployment_drift**
> Definitionsdeployment get_deployment_drift()

Get deployment drift

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentVersionApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to return

try:
    # Get deployment drift
    api_response = api_instance.get_deployment_drift()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentVersionApi->get_deployment_drift: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to return | 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_deployment_usage**
> Definitionsdeployment get_deployment_usage()

Get deployment usage

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentVersionApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to return

try:
    # Get deployment usage
    api_response = api_instance.get_deployment_usage()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentVersionApi->get_deployment_usage: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to return | 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **roll_back_deployment_version**
> Definitionsdeployment roll_back_deployment_version(, )

Rollback old version

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentVersionApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to rollback
 = swagger_client.null() #  | version of deployment to rollback

try:
    # Rollback old version
    api_response = api_instance.roll_back_deployment_version(, )
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentVersionApi->roll_back_deployment_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to rollback | 
 **** | [****](.md)| version of deployment to rollback | 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_description_by_version**
> Definitionsdeployment update_description_by_version(, )

Update description by version

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentVersionApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to rollback
 = swagger_client.null() #  | version of deployment to rollback

try:
    # Update description by version
    api_response = api_instance.update_description_by_version(, )
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentVersionApi->update_description_by_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to rollback | 
 **** | [****](.md)| version of deployment to rollback | 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

