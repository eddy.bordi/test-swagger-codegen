# ProjectIdPipelinecomponentsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**git_branch** | **str** |  | [optional] 
**git_url** | **str** |  | [optional] 
**repository_name** | **str** |  | [optional] 
**broker** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

