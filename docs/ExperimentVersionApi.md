# swagger_client.ExperimentVersionApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_preds_from_experiment**](ExperimentVersionApi.md#create_preds_from_experiment) | **POST** /experiment-versions/{experimentVersionId}/validation-predictions | create prediction on experiment-version
[**create_unit_pred_from_experiment**](ExperimentVersionApi.md#create_unit_pred_from_experiment) | **POST** /experiment-versions/{experimentVersionId}/unit-prediction | create unit prediction on experiment-version
[**edit_experiment_version_by_id**](ExperimentVersionApi.md#edit_experiment_version_by_id) | **GET** /experiment-versions/{experimentVersionId} | get experiment version
[**edit_experiment_version_by_id_0**](ExperimentVersionApi.md#edit_experiment_version_by_id_0) | **PUT** /experiment-versions/{experimentVersionId} | update experiment version description
[**get_cv_from_experiment**](ExperimentVersionApi.md#get_cv_from_experiment) | **GET** /experiment-versions/{experimentVersionId}/correlation-matrix | list experiment-version correlation matrix
[**get_dataset_validation_from_experiment**](ExperimentVersionApi.md#get_dataset_validation_from_experiment) | **GET** /experiment-versions/{experimentVersionId}/dataset-validation/{:datasetId} | compatibility
[**get_feature_by_name**](ExperimentVersionApi.md#get_feature_by_name) | **GET** /experiment-versions/{experimentVersionId}/features/{featureName} | get feature by name
[**get_features_from_experiment**](ExperimentVersionApi.md#get_features_from_experiment) | **GET** /experiment-versions/{experimentVersionId}/features | list experiment-version features
[**get_features_stats_from_experiment**](ExperimentVersionApi.md#get_features_stats_from_experiment) | **GET** /experiment-versions/{experimentVersionId}/features-stats | list experiment-version features stats
[**get_graph_from_experiment**](ExperimentVersionApi.md#get_graph_from_experiment) | **GET** /experiment-versions/{experimentVersionId}/graph | get experiment-version graph
[**get_holdout_preds_from_experiment**](ExperimentVersionApi.md#get_holdout_preds_from_experiment) | **GET** /experiment-versions/{experimentVersionId}/holdout-predictions | list experiment-version holdout predictions
[**get_image_from_experiment**](ExperimentVersionApi.md#get_image_from_experiment) | **GET** /experiment-versions/{experimentVersionId}/images/{imageName} | get image from experiment
[**get_preds_from_experiment**](ExperimentVersionApi.md#get_preds_from_experiment) | **GET** /experiment-versions/{experimentVersionId}/validation-predictions | list experiment-version predictions
[**get_task_from_experiment**](ExperimentVersionApi.md#get_task_from_experiment) | **GET** /experiment-versions/{experimentVersionId}/tasks | get experiment-version tasks
[**getdropped_features_from_experiment**](ExperimentVersionApi.md#getdropped_features_from_experiment) | **GET** /experiment-versions/{experimentVersionId}/dropped-features | list experiment-version dropped features
[**launch_experiment_version**](ExperimentVersionApi.md#launch_experiment_version) | **PUT** /experiment-versions/{experimentVersionId}/confirm | Launch experiment version
[**list_experiment_version_models**](ExperimentVersionApi.md#list_experiment_version_models) | **GET** /experiment-versions/{experimentVersionId}/models | list experiment-version models
[**pause_experiment_version_train**](ExperimentVersionApi.md#pause_experiment_version_train) | **PUT** /experiment-versions/{experimentVersionId}/pause | pause experiment-version training
[**remove_an_external_model**](ExperimentVersionApi.md#remove_an_external_model) | **DELETE** /experiment-versions/{experimentVersionId}/external-models/{externalModelId} | Remove an external model
[**resume_experiment_version_train**](ExperimentVersionApi.md#resume_experiment_version_train) | **PUT** /experiment-versions/{experimentVersionId}/resume | resume experiment-version training
[**stop_experiment_version_train**](ExperimentVersionApi.md#stop_experiment_version_train) | **PUT** /experiment-versions/{experimentVersionId}/stop | stop experiment-version training
[**upload_an_external_model**](ExperimentVersionApi.md#upload_an_external_model) | **POST** /experiment-versions/{experimentVersionId}/external-models | Upload an external model

# **create_preds_from_experiment**
> Definitionsprediction create_preds_from_experiment()

create prediction on experiment-version

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # create prediction on experiment-version
    api_response = api_instance.create_preds_from_experiment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->create_preds_from_experiment: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Definitionsprediction**](Definitionsprediction.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_unit_pred_from_experiment**
> Definitionsprediction create_unit_pred_from_experiment(body=body)

create unit prediction on experiment-version

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))
body = swagger_client.ExperimentVersionIdUnitpredictionBody() # ExperimentVersionIdUnitpredictionBody |  (optional)

try:
    # create unit prediction on experiment-version
    api_response = api_instance.create_unit_pred_from_experiment(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->create_unit_pred_from_experiment: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ExperimentVersionIdUnitpredictionBody**](ExperimentVersionIdUnitpredictionBody.md)|  | [optional] 

### Return type

[**Definitionsprediction**](Definitionsprediction.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **edit_experiment_version_by_id**
> DefinitionsexperimentVersion edit_experiment_version_by_id(body=body)

get experiment version

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))
body = swagger_client.ExperimentversionsExperimentVersionIdBody() # ExperimentversionsExperimentVersionIdBody |  (optional)

try:
    # get experiment version
    api_response = api_instance.edit_experiment_version_by_id(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->edit_experiment_version_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ExperimentversionsExperimentVersionIdBody**](ExperimentversionsExperimentVersionIdBody.md)|  | [optional] 

### Return type

[**DefinitionsexperimentVersion**](DefinitionsexperimentVersion.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **edit_experiment_version_by_id_0**
> DefinitionsexperimentVersion edit_experiment_version_by_id_0(body=body)

update experiment version description

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))
body = swagger_client.ExperimentversionsExperimentVersionIdBody1() # ExperimentversionsExperimentVersionIdBody1 |  (optional)

try:
    # update experiment version description
    api_response = api_instance.edit_experiment_version_by_id_0(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->edit_experiment_version_by_id_0: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ExperimentversionsExperimentVersionIdBody1**](ExperimentversionsExperimentVersionIdBody1.md)|  | [optional] 

### Return type

[**DefinitionsexperimentVersion**](DefinitionsexperimentVersion.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_cv_from_experiment**
> DefinitionscorrelationMatrix get_cv_from_experiment()

list experiment-version correlation matrix

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # list experiment-version correlation matrix
    api_response = api_instance.get_cv_from_experiment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->get_cv_from_experiment: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DefinitionscorrelationMatrix**](DefinitionscorrelationMatrix.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_dataset_validation_from_experiment**
> list[str] get_dataset_validation_from_experiment()

compatibility

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of dataset 

try:
    # compatibility
    api_response = api_instance.get_dataset_validation_from_experiment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->get_dataset_validation_from_experiment: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of dataset  | 

### Return type

**list[str]**

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_feature_by_name**
> DefinitionsexperimentVersionFeature get_feature_by_name()

get feature by name

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | name of feature

try:
    # get feature by name
    api_response = api_instance.get_feature_by_name()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->get_feature_by_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| name of feature | 

### Return type

[**DefinitionsexperimentVersionFeature**](DefinitionsexperimentVersionFeature.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_features_from_experiment**
> InlineResponse20021 get_features_from_experiment()

list experiment-version features

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # list experiment-version features
    api_response = api_instance.get_features_from_experiment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->get_features_from_experiment: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20021**](InlineResponse20021.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_features_stats_from_experiment**
> InlineResponse20022 get_features_stats_from_experiment()

list experiment-version features stats

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # list experiment-version features stats
    api_response = api_instance.get_features_stats_from_experiment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->get_features_stats_from_experiment: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20022**](InlineResponse20022.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_graph_from_experiment**
> Definitionsgraph get_graph_from_experiment()

get experiment-version graph

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # get experiment-version graph
    api_response = api_instance.get_graph_from_experiment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->get_graph_from_experiment: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Definitionsgraph**](Definitionsgraph.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_holdout_preds_from_experiment**
> InlineResponse20020 get_holdout_preds_from_experiment()

list experiment-version holdout predictions

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # list experiment-version holdout predictions
    api_response = api_instance.get_holdout_preds_from_experiment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->get_holdout_preds_from_experiment: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20020**](InlineResponse20020.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_image_from_experiment**
> InlineResponse20018 get_image_from_experiment()

get image from experiment

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | name of the image to return 

try:
    # get image from experiment
    api_response = api_instance.get_image_from_experiment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->get_image_from_experiment: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| name of the image to return  | 

### Return type

[**InlineResponse20018**](InlineResponse20018.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_preds_from_experiment**
> InlineResponse20019 get_preds_from_experiment()

list experiment-version predictions

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # list experiment-version predictions
    api_response = api_instance.get_preds_from_experiment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->get_preds_from_experiment: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20019**](InlineResponse20019.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_task_from_experiment**
> Definitionstask get_task_from_experiment()

get experiment-version tasks

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # get experiment-version tasks
    api_response = api_instance.get_task_from_experiment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->get_task_from_experiment: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Definitionstask**](Definitionstask.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getdropped_features_from_experiment**
> InlineResponse20021 getdropped_features_from_experiment()

list experiment-version dropped features

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # list experiment-version dropped features
    api_response = api_instance.getdropped_features_from_experiment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->getdropped_features_from_experiment: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20021**](InlineResponse20021.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **launch_experiment_version**
> DefinitionsexperimentVersion launch_experiment_version()

Launch experiment version

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # Launch experiment version
    api_response = api_instance.launch_experiment_version()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->launch_experiment_version: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DefinitionsexperimentVersion**](DefinitionsexperimentVersion.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_experiment_version_models**
> InlineResponse20023 list_experiment_version_models()

list experiment-version models

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # list experiment-version models
    api_response = api_instance.list_experiment_version_models()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->list_experiment_version_models: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20023**](InlineResponse20023.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pause_experiment_version_train**
> DefinitionsexperimentVersion pause_experiment_version_train()

pause experiment-version training

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # pause experiment-version training
    api_response = api_instance.pause_experiment_version_train()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->pause_experiment_version_train: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DefinitionsexperimentVersion**](DefinitionsexperimentVersion.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **remove_an_external_model**
> DefinitionsexperimentVersion remove_an_external_model()

Remove an external model

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of external model

try:
    # Remove an external model
    api_response = api_instance.remove_an_external_model()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->remove_an_external_model: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of external model | 

### Return type

[**DefinitionsexperimentVersion**](DefinitionsexperimentVersion.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **resume_experiment_version_train**
> DefinitionsexperimentVersion resume_experiment_version_train()

resume experiment-version training

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # resume experiment-version training
    api_response = api_instance.resume_experiment_version_train()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->resume_experiment_version_train: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DefinitionsexperimentVersion**](DefinitionsexperimentVersion.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stop_experiment_version_train**
> DefinitionsexperimentVersion stop_experiment_version_train()

stop experiment-version training

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))

try:
    # stop experiment-version training
    api_response = api_instance.stop_experiment_version_train()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->stop_experiment_version_train: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DefinitionsexperimentVersion**](DefinitionsexperimentVersion.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_an_external_model**
> DefinitionsexperimentVersion upload_an_external_model(name=name, onnx_file=onnx_file, yaml_file=yaml_file)

Upload an external model

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentVersionApi(swagger_client.ApiClient(configuration))
name = 'name_example' # str |  (optional)
onnx_file = 'onnx_file_example' # str |  (optional)
yaml_file = 'yaml_file_example' # str |  (optional)

try:
    # Upload an external model
    api_response = api_instance.upload_an_external_model(name=name, onnx_file=onnx_file, yaml_file=yaml_file)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentVersionApi->upload_an_external_model: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  | [optional] 
 **onnx_file** | **str**|  | [optional] 
 **yaml_file** | **str**|  | [optional] 

### Return type

[**DefinitionsexperimentVersion**](DefinitionsexperimentVersion.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

