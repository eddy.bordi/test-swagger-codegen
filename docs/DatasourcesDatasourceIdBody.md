# DatasourcesDatasourceIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**database** | **str** |  | [optional] 
**table** | **str** |  | [optional] 
**path** | **str** |  | [optional] 
**request** | **str** |  | [optional] 
**bucket** | **str** |  | [optional] 
**connector_id** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

