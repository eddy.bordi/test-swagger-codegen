# swagger_client.EventApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_events**](EventApi.md#get_events) | **GET** /events | 

# **get_events**
> InlineResponse200 get_events()



Prevision events link to multiples resources

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.EventApi(swagger_client.ApiClient(configuration))

try:
    api_response = api_instance.get_events()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EventApi->get_events: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

