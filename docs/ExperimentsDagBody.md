# ExperimentsDagBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**simple_models** | [**ParametersexperimentVersionsimpleModels**](ParametersexperimentVersionsimpleModels.md) |  | [optional] 
**lite_models** | [**ParametersexperimentVersionliteModels**](ParametersexperimentVersionliteModels.md) |  | [optional] 
**normal_models** | [**ParametersexperimentVersionnormalModels**](ParametersexperimentVersionnormalModels.md) |  | [optional] 
**with_blend** | [**ParametersexperimentVersionwithBlend**](ParametersexperimentVersionwithBlend.md) |  | [optional] 
**profile** | [**ParametersexperimentVersionprofile**](ParametersexperimentVersionprofile.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

