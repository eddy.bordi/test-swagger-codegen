# ConnectorstestGoogleCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** |  | [optional] 
**project_id** | **str** |  | [optional] 
**private_key_id** | **str** |  | [optional] 
**private_key** | **str** |  | [optional] 
**client_email** | **str** |  | [optional] 
**client_id** | **str** |  | [optional] 
**auth_uri** | **str** |  | [optional] 
**token_uri** | **str** |  | [optional] 
**auth_provider_x509_cert_url** | **str** |  | [optional] 
**client_x509_cert_url** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

