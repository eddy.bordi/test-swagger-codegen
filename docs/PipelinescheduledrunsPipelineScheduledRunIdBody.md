# PipelinescheduledrunsPipelineScheduledRunIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**exec_type** | **str** |  | [optional] 
**exec_cron** | **str** |  | [optional] 
**exec_period_start** | **date** |  | [optional] 
**exec_period_end** | **date** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

