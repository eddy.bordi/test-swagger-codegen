# DatasetsDatasourceBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | dataset name | [optional] 
**datasource_id** | **str** | dataset separator | [optional] 
**separator** | **str** | dataset separator | [optional] 
**decimal** | **str** | dataset char | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

