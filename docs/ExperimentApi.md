# swagger_client.ExperimentApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_experimentversion**](ExperimentApi.md#add_experimentversion) | **POST** /experiments/{experimentId}/versions | Add experiment version
[**create_experiment_dag**](ExperimentApi.md#create_experiment_dag) | **POST** /experiments/dag | create experiment dag
[**delete_experiment_by_id**](ExperimentApi.md#delete_experiment_by_id) | **DELETE** /experiments/{experimentId} | delete experiment
[**edit_experiment_by_id**](ExperimentApi.md#edit_experiment_by_id) | **PUT** /experiments/{experimentId} | edit experiment name
[**get_experiment_algorithms**](ExperimentApi.md#get_experiment_algorithms) | **GET** /experiments/algorithms | get experiment algorithms
[**get_experiment_by_id**](ExperimentApi.md#get_experiment_by_id) | **GET** /experiments/{experimentId} | get experiment
[**get_experiment_feature_engineerings**](ExperimentApi.md#get_experiment_feature_engineerings) | **GET** /experiments/feature-engineerings | get experiment algorithms
[**get_experiment_metrcis**](ExperimentApi.md#get_experiment_metrcis) | **GET** /experiments/metrics | get experiment metrics
[**get_experiment_versions_by_experiment**](ExperimentApi.md#get_experiment_versions_by_experiment) | **GET** /experiments/{experimentId}/versions | get experiment&#x27;s versions
[**get_models_by_experiment**](ExperimentApi.md#get_models_by_experiment) | **GET** /experiments/{experimentId}/models | get experiment&#x27;s models

# **add_experimentversion**
> DefinitionsexperimentVersion add_experimentversion(body=body)

Add experiment version

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentApi(swagger_client.ApiClient(configuration))
body = swagger_client.ExperimentIdVersionsBody() # ExperimentIdVersionsBody |  (optional)

try:
    # Add experiment version
    api_response = api_instance.add_experimentversion(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentApi->add_experimentversion: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ExperimentIdVersionsBody**](ExperimentIdVersionsBody.md)|  | [optional] 

### Return type

[**DefinitionsexperimentVersion**](DefinitionsexperimentVersion.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: tabular/regression, tabular/classification, tabular/multiclassification, tabular/text-similarity, images/regression, images/classification, images/multiclassification, images/object-detection, timeseries/regression, external/regression, external/classification, external/multiclassification
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_experiment_dag**
> Definitionsgraph create_experiment_dag(body=body)

create experiment dag

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentApi(swagger_client.ApiClient(configuration))
body = swagger_client.ExperimentsDagBody() # ExperimentsDagBody |  (optional)

try:
    # create experiment dag
    api_response = api_instance.create_experiment_dag(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentApi->create_experiment_dag: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ExperimentsDagBody**](ExperimentsDagBody.md)|  | [optional] 

### Return type

[**Definitionsgraph**](Definitionsgraph.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_experiment_by_id**
> delete_experiment_by_id()

delete experiment

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentApi(swagger_client.ApiClient(configuration))

try:
    # delete experiment
    api_instance.delete_experiment_by_id()
except ApiException as e:
    print("Exception when calling ExperimentApi->delete_experiment_by_id: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **edit_experiment_by_id**
> Definitionsexperiment edit_experiment_by_id(body=body)

edit experiment name

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentApi(swagger_client.ApiClient(configuration))
body = swagger_client.ExperimentsExperimentIdBody() # ExperimentsExperimentIdBody |  (optional)

try:
    # edit experiment name
    api_response = api_instance.edit_experiment_by_id(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentApi->edit_experiment_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ExperimentsExperimentIdBody**](ExperimentsExperimentIdBody.md)|  | [optional] 

### Return type

[**Definitionsexperiment**](Definitionsexperiment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_experiment_algorithms**
> object get_experiment_algorithms()

get experiment algorithms

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentApi(swagger_client.ApiClient(configuration))

try:
    # get experiment algorithms
    api_response = api_instance.get_experiment_algorithms()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentApi->get_experiment_algorithms: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_experiment_by_id**
> Definitionsexperiment get_experiment_by_id()

get experiment

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentApi(swagger_client.ApiClient(configuration))

try:
    # get experiment
    api_response = api_instance.get_experiment_by_id()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentApi->get_experiment_by_id: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Definitionsexperiment**](Definitionsexperiment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_experiment_feature_engineerings**
> object get_experiment_feature_engineerings()

get experiment algorithms

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentApi(swagger_client.ApiClient(configuration))

try:
    # get experiment algorithms
    api_response = api_instance.get_experiment_feature_engineerings()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentApi->get_experiment_feature_engineerings: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_experiment_metrcis**
> object get_experiment_metrcis()

get experiment metrics

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentApi(swagger_client.ApiClient(configuration))

try:
    # get experiment metrics
    api_response = api_instance.get_experiment_metrcis()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentApi->get_experiment_metrcis: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_experiment_versions_by_experiment**
> InlineResponse20024 get_experiment_versions_by_experiment()

get experiment's versions

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentApi(swagger_client.ApiClient(configuration))

try:
    # get experiment's versions
    api_response = api_instance.get_experiment_versions_by_experiment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentApi->get_experiment_versions_by_experiment: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20024**](InlineResponse20024.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_models_by_experiment**
> InlineResponse20025 get_models_by_experiment()

get experiment's models

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ExperimentApi(swagger_client.ApiClient(configuration))

try:
    # get experiment's models
    api_response = api_instance.get_models_by_experiment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExperimentApi->get_models_by_experiment: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20025**](InlineResponse20025.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

