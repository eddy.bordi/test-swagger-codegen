# DeploymentIdVersionsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**main_model_experiment_version_id** | **str** |  | 
**main_model_id** | **str** |  | 
**challenger_model_experiment_version_id** | **str** |  | [optional] 
**challenger_model_id** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

