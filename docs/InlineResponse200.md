# InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **datetime** |  | [optional] 
**event** | **str** |  | [optional] 
**data** | [**InlineResponse200Data**](InlineResponse200Data.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

