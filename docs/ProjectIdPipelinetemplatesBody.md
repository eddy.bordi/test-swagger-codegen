# ProjectIdPipelinetemplatesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**config_dataset_id** | **str** |  | [optional] 
**nodes** | [**list[ProjectsprojectIdpipelinetemplatesNodes]**](ProjectsprojectIdpipelinetemplatesNodes.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

