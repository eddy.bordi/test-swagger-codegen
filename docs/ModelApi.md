# swagger_client.ModelApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**download_model_cv**](ModelApi.md#download_model_cv) | **GET** /models/{modelId}/cross-validation/download | download model cross-validation matrix
[**download_model_fe**](ModelApi.md#download_model_fe) | **GET** /models/{modelId}/features-importances/download | download model features importances
[**download_model_hp**](ModelApi.md#download_model_hp) | **GET** /models/{modelId}/hyperparameters/download | download model hyperparameters
[**get_model_analysis**](ModelApi.md#get_model_analysis) | **GET** /models/{modelId}/analysis | List model featureImportances
[**get_model_by_id**](ModelApi.md#get_model_by_id) | **GET** /models/{modelId} | Find model by ID
[**get_model_fe**](ModelApi.md#get_model_fe) | **GET** /models/{modelId}/feature-importances | List model featureImportances
[**get_model_fei**](ModelApi.md#get_model_fei) | **GET** /models/{modelId}/features-engineering-importances | List model feature engineering importances
[**get_modeldyanmic**](ModelApi.md#get_modeldyanmic) | **GET** /models/{modelId}/dynamic-analysis | get model dynamic analysis
[**get_modelgain**](ModelApi.md#get_modelgain) | **GET** /models/{modelId}/gain | get model gain

# **download_model_cv**
> download_model_cv()

download model cross-validation matrix

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ModelApi()
 = swagger_client.null() #  | ID of model to return

try:
    # download model cross-validation matrix
    api_instance.download_model_cv()
except ApiException as e:
    print("Exception when calling ModelApi->download_model_cv: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of model to return | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **download_model_fe**
> download_model_fe()

download model features importances

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ModelApi()
 = swagger_client.null() #  | ID of model to return

try:
    # download model features importances
    api_instance.download_model_fe()
except ApiException as e:
    print("Exception when calling ModelApi->download_model_fe: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of model to return | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **download_model_hp**
> download_model_hp()

download model hyperparameters

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ModelApi()
 = swagger_client.null() #  | ID of model to return

try:
    # download model hyperparameters
    api_instance.download_model_hp()
except ApiException as e:
    print("Exception when calling ModelApi->download_model_hp: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of model to return | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_model_analysis**
> DefinitionsmodelAnalysis get_model_analysis()

List model featureImportances

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ModelApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of model to return

try:
    # List model featureImportances
    api_response = api_instance.get_model_analysis()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ModelApi->get_model_analysis: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of model to return | 

### Return type

[**DefinitionsmodelAnalysis**](DefinitionsmodelAnalysis.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_model_by_id**
> Definitionsmodel get_model_by_id()

Find model by ID

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ModelApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of model to return

try:
    # Find model by ID
    api_response = api_instance.get_model_by_id()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ModelApi->get_model_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of model to return | 

### Return type

[**Definitionsmodel**](Definitionsmodel.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_model_fe**
> get_model_fe()

List model featureImportances

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ModelApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of model to return

try:
    # List model featureImportances
    api_instance.get_model_fe()
except ApiException as e:
    print("Exception when calling ModelApi->get_model_fe: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of model to return | 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_model_fei**
> get_model_fei()

List model feature engineering importances

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ModelApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of model to return

try:
    # List model feature engineering importances
    api_instance.get_model_fei()
except ApiException as e:
    print("Exception when calling ModelApi->get_model_fei: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of model to return | 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_modeldyanmic**
> DefinitionsmodelDynamicAnalysis get_modeldyanmic()

get model dynamic analysis

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ModelApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of model to return

try:
    # get model dynamic analysis
    api_response = api_instance.get_modeldyanmic()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ModelApi->get_modeldyanmic: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of model to return | 

### Return type

[**DefinitionsmodelDynamicAnalysis**](DefinitionsmodelDynamicAnalysis.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_modelgain**
> DefinitionsmodelGain get_modelgain()

get model gain

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ModelApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of model to return

try:
    # get model gain
    api_response = api_instance.get_modelgain()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ModelApi->get_modelgain: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of model to return | 

### Return type

[**DefinitionsmodelGain**](DefinitionsmodelGain.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

