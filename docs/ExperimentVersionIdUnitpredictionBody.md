# ExperimentVersionIdUnitpredictionBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**model_id** | **str** |  | [optional] 
**features** | [**ExperimentversionsexperimentVersionIdunitpredictionFeatures**](ExperimentversionsexperimentVersionIdunitpredictionFeatures.md) |  | [optional] 
**confidence** | **bool** |  | [optional] 
**explain** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

