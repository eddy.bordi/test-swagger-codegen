# swagger_client.ExportersApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**exportersexporter_id_delete**](ExportersApi.md#exportersexporter_id_delete) | **DELETE** /exporters/:exporterId | delete exporter
[**exportersexporter_id_exports_get**](ExportersApi.md#exportersexporter_id_exports_get) | **GET** /exporters/:exporterId/exports | get exports from exporter
[**exportersexporter_id_get**](ExportersApi.md#exportersexporter_id_get) | **GET** /exporters/:exporterId | get exporter

# **exportersexporter_id_delete**
> exportersexporter_id_delete()

delete exporter

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ExportersApi()
 = swagger_client.null() #  | 

try:
    # delete exporter
    api_instance.exportersexporter_id_delete()
except ApiException as e:
    print("Exception when calling ExportersApi->exportersexporter_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exportersexporter_id_exports_get**
> InlineResponse20016 exportersexporter_id_exports_get(, =, =, =, =)

get exports from exporter

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ExportersApi()
 = swagger_client.null() #  | 
 = swagger_client.null() #  |  (optional)
 = swagger_client.null() #  |  (optional)
 = swagger_client.null() #  |  (optional)
 = swagger_client.null() #  |  (optional)

try:
    # get exports from exporter
    api_response = api_instance.exportersexporter_id_exports_get(, =, =, =, =)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExportersApi->exportersexporter_id_exports_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)|  | 
 **** | [****](.md)|  | [optional] 
 **** | [****](.md)|  | [optional] 
 **** | [****](.md)|  | [optional] 
 **** | [****](.md)|  | [optional] 

### Return type

[**InlineResponse20016**](InlineResponse20016.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exportersexporter_id_get**
> Definitionsexporter exportersexporter_id_get()

get exporter

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ExportersApi()
 = swagger_client.null() #  | 

try:
    # get exporter
    api_response = api_instance.exportersexporter_id_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExportersApi->exportersexporter_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)|  | 

### Return type

[**Definitionsexporter**](Definitionsexporter.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

