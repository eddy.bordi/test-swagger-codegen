# ExperimentIdVersionsBody6

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time_column** | [**ParametersexperimentVersiontimeColumn**](ParametersexperimentVersiontimeColumn.md) |  | [optional] 
**start_dw** | [**ParametersexperimentVersionstartDw**](ParametersexperimentVersionstartDw.md) |  | [optional] 
**end_dw** | [**ParametersexperimentVersionendDw**](ParametersexperimentVersionendDw.md) |  | [optional] 
**start_fw** | [**ParametersexperimentVersionstartFw**](ParametersexperimentVersionstartFw.md) |  | [optional] 
**end_fw** | [**ParametersexperimentVersionendFw**](ParametersexperimentVersionendFw.md) |  | [optional] 
**group_list** | [**ParametersexperimentVersiongroupList**](ParametersexperimentVersiongroupList.md) |  | [optional] 
**apriori_list** | [**ParametersexperimentVersionaprioriList**](ParametersexperimentVersionaprioriList.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

