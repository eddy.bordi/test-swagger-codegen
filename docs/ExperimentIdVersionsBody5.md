# ExperimentIdVersionsBody5

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**simple_models** | [**ParametersexperimentVersionsimpleModels**](ParametersexperimentVersionsimpleModels.md) |  | [optional] 
**folder_dataset_id** | [**ParametersexperimentVersionfolderDatasetId**](ParametersexperimentVersionfolderDatasetId.md) |  | [optional] 
**filename_column** | [**ParametersexperimentVersionfilenameColumn**](ParametersexperimentVersionfilenameColumn.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

