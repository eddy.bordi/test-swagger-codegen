# swagger_client.ValidationPredictionApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_prediction**](ValidationPredictionApi.md#delete_prediction) | **DELETE** /validation-predictions/{predictionId}/download | delete predictions
[**download_prediction**](ValidationPredictionApi.md#download_prediction) | **GET** /validation-predictions/{predictionId}/download | download prediction
[**get_prediction_by_id**](ValidationPredictionApi.md#get_prediction_by_id) | **GET** /validation-predictions/{predictionId} | get prediction
[**get_prediction_explanations**](ValidationPredictionApi.md#get_prediction_explanations) | **POST** /validation-predictions/{predictionId}/explanations | get prediction explanations 
[**get_prediction_sample**](ValidationPredictionApi.md#get_prediction_sample) | **GET** /validation-predictions/{predictionId}/sample | get prediction sample 

# **delete_prediction**
> delete_prediction()

delete predictions

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ValidationPredictionApi()

try:
    # delete predictions
    api_instance.delete_prediction()
except ApiException as e:
    print("Exception when calling ValidationPredictionApi->delete_prediction: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **download_prediction**
> download_prediction(, =)

download prediction

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ValidationPredictionApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of prediction to return
 = swagger_client.null() #  | select main or challenger model_type (optional)

try:
    # download prediction
    api_instance.download_prediction(, =)
except ApiException as e:
    print("Exception when calling ValidationPredictionApi->download_prediction: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of prediction to return | 
 **** | [****](.md)| select main or challenger model_type | [optional] 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_prediction_by_id**
> get_prediction_by_id()

get prediction

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ValidationPredictionApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of prediction to return

try:
    # get prediction
    api_instance.get_prediction_by_id()
except ApiException as e:
    print("Exception when calling ValidationPredictionApi->get_prediction_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of prediction to return | 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_prediction_explanations**
> DefinitionsvalidationPrediction get_prediction_explanations()

get prediction explanations 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ValidationPredictionApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of prediction to return

try:
    # get prediction explanations 
    api_response = api_instance.get_prediction_explanations()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValidationPredictionApi->get_prediction_explanations: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of prediction to return | 

### Return type

[**DefinitionsvalidationPrediction**](DefinitionsvalidationPrediction.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_prediction_sample**
> get_prediction_sample()

get prediction sample 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ValidationPredictionApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of prediction to return

try:
    # get prediction sample 
    api_instance.get_prediction_sample()
except ApiException as e:
    print("Exception when calling ValidationPredictionApi->get_prediction_sample: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of prediction to return | 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

