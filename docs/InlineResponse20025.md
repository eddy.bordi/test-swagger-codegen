# InlineResponse20025

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**list[DefinitionsmetaData]**](DefinitionsmetaData.md) |  | [optional] 
**meta_data** | [**Definitionsmodel**](Definitionsmodel.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

