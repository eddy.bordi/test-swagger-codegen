# swagger_client.PipelineRunApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_pipeline_run**](PipelineRunApi.md#get_pipeline_run) | **GET** /pipeline-runs/{pipelineRunId} | get pipeline-run

# **get_pipeline_run**
> DefinitionspipelineRun get_pipeline_run()

get pipeline-run

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PipelineRunApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of pipeline-run to return

try:
    # get pipeline-run
    api_response = api_instance.get_pipeline_run()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineRunApi->get_pipeline_run: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of pipeline-run to return | 

### Return type

[**DefinitionspipelineRun**](DefinitionspipelineRun.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

