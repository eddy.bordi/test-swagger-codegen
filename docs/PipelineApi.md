# swagger_client.PipelineApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_pipeline_component_in_template**](PipelineApi.md#add_pipeline_component_in_template) | **POST** /pipelines/{pipelineId}/node | add a node to a pipeline, the node is automaticaly added at the end pf the pipeline
[**confirm_pipeline_template**](PipelineApi.md#confirm_pipeline_template) | **POST** /pipeline-templates/{pipelineId}/confirm | confirm pipeline template and save in kubeflow
[**delete_pipeline**](PipelineApi.md#delete_pipeline) | **DELETE** /pipeline-templates/{pipelineId} | delete pipelines
[**get_pipeline**](PipelineApi.md#get_pipeline) | **GET** /pipeline-templates/{pipelineId} | get pipeline

# **add_pipeline_component_in_template**
> Definitionspipeline add_pipeline_component_in_template()

add a node to a pipeline, the node is automaticaly added at the end pf the pipeline

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PipelineApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of pipeline to edit

try:
    # add a node to a pipeline, the node is automaticaly added at the end pf the pipeline
    api_response = api_instance.add_pipeline_component_in_template()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineApi->add_pipeline_component_in_template: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of pipeline to edit | 

### Return type

[**Definitionspipeline**](Definitionspipeline.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **confirm_pipeline_template**
> Definitionspipeline confirm_pipeline_template()

confirm pipeline template and save in kubeflow

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PipelineApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of pipeline to edit

try:
    # confirm pipeline template and save in kubeflow
    api_response = api_instance.confirm_pipeline_template()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineApi->confirm_pipeline_template: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of pipeline to edit | 

### Return type

[**Definitionspipeline**](Definitionspipeline.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_pipeline**
> delete_pipeline()

delete pipelines

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PipelineApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of pipeline to return

try:
    # delete pipelines
    api_instance.delete_pipeline()
except ApiException as e:
    print("Exception when calling PipelineApi->delete_pipeline: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of pipeline to return | 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pipeline**
> Definitionspipeline get_pipeline()

get pipeline

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PipelineApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of pipeline to return

try:
    # get pipeline
    api_response = api_instance.get_pipeline()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PipelineApi->get_pipeline: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of pipeline to return | 

### Return type

[**Definitionspipeline**](Definitionspipeline.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

