# ConnectorsTestBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | connector type | [optional] 
**username** | **str** | connector username | [optional] 
**password** | **str** | connector password | [optional] 
**port** | **str** | connector port | [optional] 
**host** | **str** | connector host | [optional] 
**google_credentials** | [**ConnectorstestGoogleCredentials**](ConnectorstestGoogleCredentials.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

