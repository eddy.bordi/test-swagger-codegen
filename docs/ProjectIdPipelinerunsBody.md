# ProjectIdPipelinerunsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pipeline_id** | **str** |  | [optional] 
**pipeline_parameters** | [**ProjectsprojectIdpipelinerunsPipelineParameters**](ProjectsprojectIdpipelinerunsPipelineParameters.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

