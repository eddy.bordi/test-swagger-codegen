# ExporterIdFileBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**origin** | **str** |  | [optional] [default to 'file']
**file** | [**Object**](Object.md) | (required) | [optional] 
**pipeline_scheduled_run_id** | **str** |  | [optional] 
**encoding** | **str** |  | [optional] 
**separator** | **str** |  | [optional] 
**decimal** | [**Object**](Object.md) |  | [optional] 
**thousands** | [**Object**](Object.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

