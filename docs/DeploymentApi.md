# swagger_client.DeploymentApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_deployment_api_keys**](DeploymentApi.md#create_deployment_api_keys) | **POST** /deployments/{deploymentId}/api-keys | Create deployment api keys
[**get_deployment**](DeploymentApi.md#get_deployment) | **GET** /deployments/{deploymentId} | Get one deployment
[**get_deployment_api_keys**](DeploymentApi.md#get_deployment_api_keys) | **GET** /deployments/{deploymentId}/api-keys | Get deployment api keys
[**get_deployment_build_logs**](DeploymentApi.md#get_deployment_build_logs) | **GET** /deployments/{deploymentId}/logs/build | Get deployment build logs
[**get_deployment_deploy_logs**](DeploymentApi.md#get_deployment_deploy_logs) | **GET** /deployments/{deploymentId}/logs/deploy | Get deployment deploy logs
[**get_deployment_feature_distribution**](DeploymentApi.md#get_deployment_feature_distribution) | **GET** /deployments/{deploymentId}/feature-distribution | Get deployment features distribution
[**get_deployment_features**](DeploymentApi.md#get_deployment_features) | **GET** /deployments/{deploymentId}/features | List deployment features
[**get_deployment_run_logs**](DeploymentApi.md#get_deployment_run_logs) | **GET** /deployments/{deploymentId}/logs/run | Get deployment run logs
[**get_deployment_variables**](DeploymentApi.md#get_deployment_variables) | **GET** /deployments/{deploymentId}/variables | Get deployment env variables
[**list_deployment_predictions**](DeploymentApi.md#list_deployment_predictions) | **GET** /deployments/{deploymentId}/deployment-predictions | List deployment predictions
[**list_deployments**](DeploymentApi.md#list_deployments) | **GET** /deployments | Get list of deployments by user
[**list_deployments_visible**](DeploymentApi.md#list_deployments_visible) | **GET** /deployments/visible | Get list of visibles deployments
[**remove_deployment**](DeploymentApi.md#remove_deployment) | **DELETE** /deployments/{deploymentId} | Remove deployment

# **create_deployment_api_keys**
> Definitionsdeployment create_deployment_api_keys()

Create deployment api keys

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to return

try:
    # Create deployment api keys
    api_response = api_instance.create_deployment_api_keys()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->create_deployment_api_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to return | 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_deployment**
> Definitionsdeployment get_deployment()

Get one deployment

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to return

try:
    # Get one deployment
    api_response = api_instance.get_deployment()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->get_deployment: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to return | 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_deployment_api_keys**
> Definitionsdeployment get_deployment_api_keys()

Get deployment api keys

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to return

try:
    # Get deployment api keys
    api_response = api_instance.get_deployment_api_keys()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->get_deployment_api_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to return | 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_deployment_build_logs**
> Definitionsdeployment get_deployment_build_logs()

Get deployment build logs

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to return

try:
    # Get deployment build logs
    api_response = api_instance.get_deployment_build_logs()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->get_deployment_build_logs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to return | 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_deployment_deploy_logs**
> Definitionsdeployment get_deployment_deploy_logs()

Get deployment deploy logs

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to return

try:
    # Get deployment deploy logs
    api_response = api_instance.get_deployment_deploy_logs()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->get_deployment_deploy_logs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to return | 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_deployment_feature_distribution**
> Definitionsdeployment get_deployment_feature_distribution()

Get deployment features distribution

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to return

try:
    # Get deployment features distribution
    api_response = api_instance.get_deployment_feature_distribution()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->get_deployment_feature_distribution: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to return | 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_deployment_features**
> Definitionsdeployment get_deployment_features()

List deployment features

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to return

try:
    # List deployment features
    api_response = api_instance.get_deployment_features()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->get_deployment_features: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to return | 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_deployment_run_logs**
> Definitionsdeployment get_deployment_run_logs()

Get deployment run logs

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to return

try:
    # Get deployment run logs
    api_response = api_instance.get_deployment_run_logs()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->get_deployment_run_logs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to return | 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_deployment_variables**
> Definitionsdeployment get_deployment_variables()

Get deployment env variables

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to return

try:
    # Get deployment env variables
    api_response = api_instance.get_deployment_variables()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->get_deployment_variables: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to return | 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_deployment_predictions**
> InlineResponse20013 list_deployment_predictions()

List deployment predictions

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to return

try:
    # List deployment predictions
    api_response = api_instance.list_deployment_predictions()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->list_deployment_predictions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to return | 

### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_deployments**
> InlineResponse20010 list_deployments()

Get list of deployments by user

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentApi(swagger_client.ApiClient(configuration))

try:
    # Get list of deployments by user
    api_response = api_instance.list_deployments()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->list_deployments: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_deployments_visible**
> InlineResponse20010 list_deployments_visible()

Get list of visibles deployments

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentApi(swagger_client.ApiClient(configuration))

try:
    # Get list of visibles deployments
    api_response = api_instance.list_deployments_visible()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeploymentApi->list_deployments_visible: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **remove_deployment**
> remove_deployment()

Remove deployment

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DeploymentApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of deployment to delete

try:
    # Remove deployment
    api_instance.remove_deployment()
except ApiException as e:
    print("Exception when calling DeploymentApi->remove_deployment: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of deployment to delete | 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

