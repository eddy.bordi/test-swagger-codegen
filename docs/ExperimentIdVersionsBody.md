# ExperimentIdVersionsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**simple_models** | [**ParametersexperimentVersionsimpleModels**](ParametersexperimentVersionsimpleModels.md) |  | [optional] 
**holdout_dataset_id** | [**ParametersexperimentVersionholdoutDatasetId**](ParametersexperimentVersionholdoutDatasetId.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

