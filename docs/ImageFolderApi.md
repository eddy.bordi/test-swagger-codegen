# swagger_client.ImageFolderApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_dataset**](ImageFolderApi.md#delete_dataset) | **DELETE** /image-foldes/{imageFolderId} | Delete image-folder
[**download_image_folder**](ImageFolderApi.md#download_image_folder) | **GET** /image-folders/{imageFolderId}/download | Download image-folder
[**get_image_folder**](ImageFolderApi.md#get_image_folder) | **GET** /image-foldes/{imageFolderId} | Get image-folder

# **delete_dataset**
> delete_dataset()

Delete image-folder

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ImageFolderApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of imageFolder to return

try:
    # Delete image-folder
    api_instance.delete_dataset()
except ApiException as e:
    print("Exception when calling ImageFolderApi->delete_dataset: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of imageFolder to return | 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **download_image_folder**
> download_image_folder()

Download image-folder

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ImageFolderApi()
 = swagger_client.null() #  | ID of dataset to return

try:
    # Download image-folder
    api_instance.download_image_folder()
except ApiException as e:
    print("Exception when calling ImageFolderApi->download_image_folder: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of dataset to return | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_image_folder**
> DefinitionsimageFolder get_image_folder()

Get image-folder

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ImageFolderApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of imageFolder to return

try:
    # Get image-folder
    api_response = api_instance.get_image_folder()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ImageFolderApi->get_image_folder: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of imageFolder to return | 

### Return type

[**DefinitionsimageFolder**](DefinitionsimageFolder.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

