# ProjectIdExportersBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**connector_id** | **str** |  | [optional] 
**database** | **str** | only if used with a database system connector | [optional] 
**table** | **str** | only if used with a database system connector | [optional] 
**database_write_mode** | **str** | only if used with a database system connector | [optional] 
**filepath** | **str** | only if used with a file system connector | [optional] 
**file_write_mode** | **str** | only if used with a file system connector | [optional] 
**bucket** | **str** | only if used with GCP Storage or S3 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

