# swagger_client.ExportsApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**exportsexport_id_get**](ExportsApi.md#exportsexport_id_get) | **GET** /exports/:exportId | get export

# **exportsexport_id_get**
> Definitionsexport exportsexport_id_get()

get export

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ExportsApi()
 = swagger_client.null() #  | 

try:
    # get export
    api_response = api_instance.exportsexport_id_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ExportsApi->exportsexport_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)|  | 

### Return type

[**Definitionsexport**](Definitionsexport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

