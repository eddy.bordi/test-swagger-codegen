# swagger_client.DataSourceApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**data_source_connection_test**](DataSourceApi.md#data_source_connection_test) | **POST** /data-sources/{datasourceId}/test | Data source connection test
[**get_data_source**](DataSourceApi.md#get_data_source) | **GET** /data-sources/{datasourceId} | Get data-source
[**new_data_source_connection_test**](DataSourceApi.md#new_data_source_connection_test) | **POST** /data-sources/test | New data source connection test
[**remove_data_source**](DataSourceApi.md#remove_data_source) | **DELETE** /data-sources/{datasourceId} | Remove data-source

# **data_source_connection_test**
> InlineResponse20011 data_source_connection_test()

Data source connection test

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DataSourceApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of datasource to return

try:
    # Data source connection test
    api_response = api_instance.data_source_connection_test()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DataSourceApi->data_source_connection_test: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of datasource to return | 

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_data_source**
> DefinitionsdataSource get_data_source()

Get data-source

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DataSourceApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of data-source to return

try:
    # Get data-source
    api_response = api_instance.get_data_source()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DataSourceApi->get_data_source: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of data-source to return | 

### Return type

[**DefinitionsdataSource**](DefinitionsdataSource.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **new_data_source_connection_test**
> InlineResponse20011 new_data_source_connection_test(body=body)

New data source connection test

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DataSourceApi(swagger_client.ApiClient(configuration))
body = swagger_client.DatasourcesTestBody() # DatasourcesTestBody |  (optional)

try:
    # New data source connection test
    api_response = api_instance.new_data_source_connection_test(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DataSourceApi->new_data_source_connection_test: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DatasourcesTestBody**](DatasourcesTestBody.md)|  | [optional] 

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **remove_data_source**
> remove_data_source()

Remove data-source

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DataSourceApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of data-source to delete

try:
    # Remove data-source
    api_instance.remove_data_source()
except ApiException as e:
    print("Exception when calling DataSourceApi->remove_data_source: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of data-source to delete | 

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

