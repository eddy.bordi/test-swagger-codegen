# swagger_client.ProjectApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_application_deployment_to_project**](ProjectApi.md#add_application_deployment_to_project) | **POST** /projects/{projectId}/application-deployments | Add application deployment to project
[**add_connector_to_project**](ProjectApi.md#add_connector_to_project) | **POST** /projects/{projectId}/connectors | Create a new connector to project
[**add_data_source_to_project**](ProjectApi.md#add_data_source_to_project) | **POST** /projects/{projectId}/data-sources | Add new datasource in project
[**add_dataset_file_to_project**](ProjectApi.md#add_dataset_file_to_project) | **POST** /projects/{projectId}/datasets/file | Add new dataset from file in project
[**add_dataset_from_data_source_to_project**](ProjectApi.md#add_dataset_from_data_source_to_project) | **POST** /projects/{projectId}/datasets/data-source | Add new dataset from datasource in project
[**add_image_folder_to_project**](ProjectApi.md#add_image_folder_to_project) | **POST** /projects/{projectId}/image-folders | Add new image-folder in project
[**add_model_deployment_to_project**](ProjectApi.md#add_model_deployment_to_project) | **POST** /projects/{projectId}/model-deployments | Add model deployment to project
[**add_pipeline_component_to_project**](ProjectApi.md#add_pipeline_component_to_project) | **POST** /projects/{projectId}/pipeline-components | Add pipeline-component to project
[**add_pipeline_run_to_project**](ProjectApi.md#add_pipeline_run_to_project) | **POST** /projects/{projectId}/pipeline-runs | Add pipeline-run to project
[**add_pipeline_to_project**](ProjectApi.md#add_pipeline_to_project) | **POST** /projects/{projectId}/pipeline-templates | Add pipeline to project
[**add_user_in_project**](ProjectApi.md#add_user_in_project) | **POST** /projects/{projectId}/users | Add new user in project
[**create_experiment**](ProjectApi.md#create_experiment) | **POST** /projects/{projectId}/experiments | Create experiment
[**create_project**](ProjectApi.md#create_project) | **POST** /projects | Create a new project
[**edit_project**](ProjectApi.md#edit_project) | **PUT** /projects/{projectId} | Edit project
[**get_application_deployments_by_project**](ProjectApi.md#get_application_deployments_by_project) | **GET** /projects/{projectId}/application-deployments | List project&#x27;s application deployments
[**get_connectors_by_project**](ProjectApi.md#get_connectors_by_project) | **GET** /projects/{projectId}/connectors | List project connectors
[**get_data_sources_by_project**](ProjectApi.md#get_data_sources_by_project) | **GET** /projects/{projectId}/data-sources | List project data-sources
[**get_datasets_by_project**](ProjectApi.md#get_datasets_by_project) | **GET** /projects/{projectId}/datasets | List project datasets
[**get_experiments_by_project**](ProjectApi.md#get_experiments_by_project) | **GET** /projects/{projectId}/experiments | List project experiments
[**get_image_folder_by_project**](ProjectApi.md#get_image_folder_by_project) | **GET** /projects/{projectId}/image-folders | List project image-folders
[**get_model_deployments_by_project**](ProjectApi.md#get_model_deployments_by_project) | **GET** /projects/{projectId}/model-deployments | List project&#x27;s model deployments
[**get_pipeline_components_by_project**](ProjectApi.md#get_pipeline_components_by_project) | **GET** /projects/{projectId}/pipeline-components | List project&#x27;s pipeline-components
[**get_pipelines_by_project**](ProjectApi.md#get_pipelines_by_project) | **GET** /projects/{projectId}/pipeline-templates | List project&#x27;s pipeline
[**get_pipelines_name_by_project**](ProjectApi.md#get_pipelines_name_by_project) | **GET** /projects/{projectId}/pipelines/names | List project&#x27;s pipeline&#x27;s names
[**get_pipelines_run_by_project**](ProjectApi.md#get_pipelines_run_by_project) | **GET** /projects/{projectId}/pipeline-runs | List project&#x27;s pipeline-runs
[**get_project_by_id**](ProjectApi.md#get_project_by_id) | **GET** /projects/{projectId} | Find project by ID
[**get_projects**](ProjectApi.md#get_projects) | **GET** /projects | Returns list of projects
[**get_users_project_by_id**](ProjectApi.md#get_users_project_by_id) | **GET** /projects/{projectId}/users | Find project users
[**projectsproject_id_exporters_get**](ProjectApi.md#projectsproject_id_exporters_get) | **GET** /projects/:projectId/exporters | get exporters in a project
[**remove_user_in_project**](ProjectApi.md#remove_user_in_project) | **DELETE** /projects/{projectId}/users/{user_id} | Remove user in project

# **add_application_deployment_to_project**
> Definitionsdeployment add_application_deployment_to_project(body=body)

Add application deployment to project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))
body = swagger_client.ProjectIdApplicationdeploymentsBody() # ProjectIdApplicationdeploymentsBody |  (optional)

try:
    # Add application deployment to project
    api_response = api_instance.add_application_deployment_to_project(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->add_application_deployment_to_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdApplicationdeploymentsBody**](ProjectIdApplicationdeploymentsBody.md)|  | [optional] 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_connector_to_project**
> Definitionsconnector add_connector_to_project()

Create a new connector to project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # Create a new connector to project
    api_response = api_instance.add_connector_to_project()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->add_connector_to_project: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Definitionsconnector**](Definitionsconnector.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_data_source_to_project**
> DefinitionsdataSource add_data_source_to_project()

Add new datasource in project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # Add new datasource in project
    api_response = api_instance.add_data_source_to_project()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->add_data_source_to_project: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DefinitionsdataSource**](DefinitionsdataSource.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_dataset_file_to_project**
> Definitionsdataset add_dataset_file_to_project(body=body)

Add new dataset from file in project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))
body = swagger_client.DatasetsFileBody() # DatasetsFileBody |  (optional)

try:
    # Add new dataset from file in project
    api_response = api_instance.add_dataset_file_to_project(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->add_dataset_file_to_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DatasetsFileBody**](DatasetsFileBody.md)|  | [optional] 

### Return type

[**Definitionsdataset**](Definitionsdataset.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_dataset_from_data_source_to_project**
> Definitionsdataset add_dataset_from_data_source_to_project(body=body)

Add new dataset from datasource in project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))
body = swagger_client.DatasetsDatasourceBody() # DatasetsDatasourceBody |  (optional)

try:
    # Add new dataset from datasource in project
    api_response = api_instance.add_dataset_from_data_source_to_project(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->add_dataset_from_data_source_to_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DatasetsDatasourceBody**](DatasetsDatasourceBody.md)|  | [optional] 

### Return type

[**Definitionsdataset**](Definitionsdataset.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_image_folder_to_project**
> DefinitionsimageFolder add_image_folder_to_project(body=body)

Add new image-folder in project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))
body = swagger_client.ProjectIdImagefoldersBody() # ProjectIdImagefoldersBody |  (optional)

try:
    # Add new image-folder in project
    api_response = api_instance.add_image_folder_to_project(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->add_image_folder_to_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdImagefoldersBody**](ProjectIdImagefoldersBody.md)|  | [optional] 

### Return type

[**DefinitionsimageFolder**](DefinitionsimageFolder.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_model_deployment_to_project**
> Definitionsdeployment add_model_deployment_to_project(body=body)

Add model deployment to project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))
body = swagger_client.ProjectIdModeldeploymentsBody() # ProjectIdModeldeploymentsBody |  (optional)

try:
    # Add model deployment to project
    api_response = api_instance.add_model_deployment_to_project(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->add_model_deployment_to_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdModeldeploymentsBody**](ProjectIdModeldeploymentsBody.md)|  | [optional] 

### Return type

[**Definitionsdeployment**](Definitionsdeployment.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_pipeline_component_to_project**
> DefinitionspipelineComponent add_pipeline_component_to_project(body=body)

Add pipeline-component to project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))
body = swagger_client.ProjectIdPipelinecomponentsBody() # ProjectIdPipelinecomponentsBody |  (optional)

try:
    # Add pipeline-component to project
    api_response = api_instance.add_pipeline_component_to_project(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->add_pipeline_component_to_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdPipelinecomponentsBody**](ProjectIdPipelinecomponentsBody.md)|  | [optional] 

### Return type

[**DefinitionspipelineComponent**](DefinitionspipelineComponent.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_pipeline_run_to_project**
> DefinitionspipelineRun add_pipeline_run_to_project(body=body)

Add pipeline-run to project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))
body = swagger_client.ProjectIdPipelinerunsBody() # ProjectIdPipelinerunsBody |  (optional)

try:
    # Add pipeline-run to project
    api_response = api_instance.add_pipeline_run_to_project(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->add_pipeline_run_to_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdPipelinerunsBody**](ProjectIdPipelinerunsBody.md)|  | [optional] 

### Return type

[**DefinitionspipelineRun**](DefinitionspipelineRun.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_pipeline_to_project**
> Definitionspipeline add_pipeline_to_project(body=body)

Add pipeline to project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))
body = swagger_client.ProjectIdPipelinetemplatesBody() # ProjectIdPipelinetemplatesBody |  (optional)

try:
    # Add pipeline to project
    api_response = api_instance.add_pipeline_to_project(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->add_pipeline_to_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdPipelinetemplatesBody**](ProjectIdPipelinetemplatesBody.md)|  | [optional] 

### Return type

[**Definitionspipeline**](Definitionspipeline.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_user_in_project**
> Definitionsproject add_user_in_project(body=body)

Add new user in project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))
body = swagger_client.ProjectIdUsersBody() # ProjectIdUsersBody |  (optional)

try:
    # Add new user in project
    api_response = api_instance.add_user_in_project(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->add_user_in_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdUsersBody**](ProjectIdUsersBody.md)|  | [optional] 

### Return type

[**Definitionsproject**](Definitionsproject.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_experiment**
> create_experiment(body=body)

Create experiment

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ProjectApi()
body = swagger_client.ProjectIdExperimentsBody() # ProjectIdExperimentsBody |  (optional)

try:
    # Create experiment
    api_instance.create_experiment(body=body)
except ApiException as e:
    print("Exception when calling ProjectApi->create_experiment: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdExperimentsBody**](ProjectIdExperimentsBody.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_project**
> Definitionsproject create_project(body=body)

Create a new project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))
body = swagger_client.ProjectsBody() # ProjectsBody |  (optional)

try:
    # Create a new project
    api_response = api_instance.create_project(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->create_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectsBody**](ProjectsBody.md)|  | [optional] 

### Return type

[**Definitionsproject**](Definitionsproject.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **edit_project**
> Definitionsproject edit_project(body=body)

Edit project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))
body = swagger_client.ProjectsProjectIdBody() # ProjectsProjectIdBody |  (optional)

try:
    # Edit project
    api_response = api_instance.edit_project(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->edit_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectsProjectIdBody**](ProjectsProjectIdBody.md)|  | [optional] 

### Return type

[**Definitionsproject**](Definitionsproject.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_application_deployments_by_project**
> InlineResponse20010 get_application_deployments_by_project()

List project's application deployments

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # List project's application deployments
    api_response = api_instance.get_application_deployments_by_project()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->get_application_deployments_by_project: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_connectors_by_project**
> InlineResponse2002 get_connectors_by_project()

List project connectors

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # List project connectors
    api_response = api_instance.get_connectors_by_project()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->get_connectors_by_project: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_data_sources_by_project**
> InlineResponse2005 get_data_sources_by_project()

List project data-sources

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # List project data-sources
    api_response = api_instance.get_data_sources_by_project()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->get_data_sources_by_project: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_datasets_by_project**
> InlineResponse2003 get_datasets_by_project()

List project datasets

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # List project datasets
    api_response = api_instance.get_datasets_by_project()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->get_datasets_by_project: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_experiments_by_project**
> InlineResponse2006 get_experiments_by_project()

List project experiments

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # List project experiments
    api_response = api_instance.get_experiments_by_project()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->get_experiments_by_project: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_image_folder_by_project**
> InlineResponse2004 get_image_folder_by_project()

List project image-folders

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # List project image-folders
    api_response = api_instance.get_image_folder_by_project()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->get_image_folder_by_project: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_model_deployments_by_project**
> InlineResponse20010 get_model_deployments_by_project()

List project's model deployments

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # List project's model deployments
    api_response = api_instance.get_model_deployments_by_project()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->get_model_deployments_by_project: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pipeline_components_by_project**
> InlineResponse2007 get_pipeline_components_by_project()

List project's pipeline-components

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # List project's pipeline-components
    api_response = api_instance.get_pipeline_components_by_project()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->get_pipeline_components_by_project: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pipelines_by_project**
> InlineResponse2008 get_pipelines_by_project()

List project's pipeline

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # List project's pipeline
    api_response = api_instance.get_pipelines_by_project()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->get_pipelines_by_project: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pipelines_name_by_project**
> get_pipelines_name_by_project()

List project's pipeline's names

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # List project's pipeline's names
    api_instance.get_pipelines_name_by_project()
except ApiException as e:
    print("Exception when calling ProjectApi->get_pipelines_name_by_project: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pipelines_run_by_project**
> InlineResponse2009 get_pipelines_run_by_project()

List project's pipeline-runs

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # List project's pipeline-runs
    api_response = api_instance.get_pipelines_run_by_project()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->get_pipelines_run_by_project: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2009**](InlineResponse2009.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_project_by_id**
> Definitionsproject get_project_by_id()

Find project by ID

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # Find project by ID
    api_response = api_instance.get_project_by_id()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->get_project_by_id: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Definitionsproject**](Definitionsproject.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_projects**
> InlineResponse2001 get_projects()

Returns list of projects

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # Returns list of projects
    api_response = api_instance.get_projects()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->get_projects: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_users_project_by_id**
> Definitionsproject get_users_project_by_id()

Find project users

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))

try:
    # Find project users
    api_response = api_instance.get_users_project_by_id()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->get_users_project_by_id: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Definitionsproject**](Definitionsproject.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projectsproject_id_exporters_get**
> InlineResponse20017 projectsproject_id_exporters_get(, =, =, =, =, =)

get exporters in a project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ProjectApi()
 = swagger_client.null() #  | 
 = swagger_client.null() #  |  (optional)
 = swagger_client.null() #  |  (optional)
 = swagger_client.null() #  |  (optional)
 = swagger_client.null() #  |  (optional)
 = swagger_client.null() #  |  (optional)

try:
    # get exporters in a project
    api_response = api_instance.projectsproject_id_exporters_get(, =, =, =, =, =)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->projectsproject_id_exporters_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)|  | 
 **** | [****](.md)|  | [optional] 
 **** | [****](.md)|  | [optional] 
 **** | [****](.md)|  | [optional] 
 **** | [****](.md)|  | [optional] 
 **** | [****](.md)|  | [optional] 

### Return type

[**InlineResponse20017**](InlineResponse20017.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **remove_user_in_project**
> Definitionsproject remove_user_in_project()

Remove user in project

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | id of user to patch

try:
    # Remove user in project
    api_response = api_instance.remove_user_in_project()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectApi->remove_user_in_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| id of user to patch | 

### Return type

[**Definitionsproject**](Definitionsproject.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

