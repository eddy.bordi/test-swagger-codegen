# ProjectIdExperimentsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data_type** | [**ParametersexperimentdataType**](ParametersexperimentdataType.md) |  | [optional] 
**training_type** | [**ParametersexperimenttrainingType**](ParametersexperimenttrainingType.md) |  | [optional] 
**name** | [**Parametersexperimentname**](Parametersexperimentname.md) |  | [optional] 
**provider** | [**Parametersexperimentprovider**](Parametersexperimentprovider.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

