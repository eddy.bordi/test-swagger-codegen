# swagger_client.DatasetColumnApi

All URIs are relative to *https://eddy-bordi.dev.c2.bel1.kube.pvio.tech/ext/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_dataset_column_by_id**](DatasetColumnApi.md#get_dataset_column_by_id) | **GET** /dataset-columns/{columnId} | Find dataset column by ID

# **get_dataset_column_by_id**
> DefinitionsdatasetColumn get_dataset_column_by_id()

Find dataset column by ID

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: master_token
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DatasetColumnApi(swagger_client.ApiClient(configuration))
 = swagger_client.null() #  | ID of dataset column to return

try:
    # Find dataset column by ID
    api_response = api_instance.get_dataset_column_by_id()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DatasetColumnApi->get_dataset_column_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **** | [****](.md)| ID of dataset column to return | 

### Return type

[**DefinitionsdatasetColumn**](DefinitionsdatasetColumn.md)

### Authorization

[master_token](../README.md#master_token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

