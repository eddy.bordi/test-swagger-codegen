# PredictionPredictionIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prediction_type** | **str** |  | [optional] 
**pipeline_scheduled_run_id** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

