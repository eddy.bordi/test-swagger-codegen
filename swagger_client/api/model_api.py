# coding: utf-8

"""
    Prevision.io

    Prevision.io Studio APIs  # noqa: E501

    OpenAPI spec version: 1.0.5
    Contact: web@prevision.io
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from swagger_client.api_client import ApiClient


class ModelApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def download_model_cv(self, , **kwargs):  # noqa: E501
        """download model cross-validation matrix  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.download_model_cv(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.download_model_cv_with_http_info(, **kwargs)  # noqa: E501
        else:
            (data) = self.download_model_cv_with_http_info(, **kwargs)  # noqa: E501
            return data

    def download_model_cv_with_http_info(self, , **kwargs):  # noqa: E501
        """download model cross-validation matrix  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.download_model_cv_with_http_info(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method download_model_cv" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter '' is set
        if ('' not in params or
                params[''] is None):
            raise ValueError("Missing the required parameter `` when calling `download_model_cv`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if '' in params:
            path_params['modelId'] = params['']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/models/{modelId}/cross-validation/download', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def download_model_fe(self, , **kwargs):  # noqa: E501
        """download model features importances  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.download_model_fe(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.download_model_fe_with_http_info(, **kwargs)  # noqa: E501
        else:
            (data) = self.download_model_fe_with_http_info(, **kwargs)  # noqa: E501
            return data

    def download_model_fe_with_http_info(self, , **kwargs):  # noqa: E501
        """download model features importances  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.download_model_fe_with_http_info(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method download_model_fe" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter '' is set
        if ('' not in params or
                params[''] is None):
            raise ValueError("Missing the required parameter `` when calling `download_model_fe`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if '' in params:
            path_params['modelId'] = params['']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/models/{modelId}/features-importances/download', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def download_model_hp(self, , **kwargs):  # noqa: E501
        """download model hyperparameters  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.download_model_hp(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.download_model_hp_with_http_info(, **kwargs)  # noqa: E501
        else:
            (data) = self.download_model_hp_with_http_info(, **kwargs)  # noqa: E501
            return data

    def download_model_hp_with_http_info(self, , **kwargs):  # noqa: E501
        """download model hyperparameters  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.download_model_hp_with_http_info(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method download_model_hp" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter '' is set
        if ('' not in params or
                params[''] is None):
            raise ValueError("Missing the required parameter `` when calling `download_model_hp`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if '' in params:
            path_params['modelId'] = params['']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/models/{modelId}/hyperparameters/download', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_model_analysis(self, , **kwargs):  # noqa: E501
        """List model featureImportances  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_model_analysis(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: DefinitionsmodelAnalysis
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_model_analysis_with_http_info(, **kwargs)  # noqa: E501
        else:
            (data) = self.get_model_analysis_with_http_info(, **kwargs)  # noqa: E501
            return data

    def get_model_analysis_with_http_info(self, , **kwargs):  # noqa: E501
        """List model featureImportances  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_model_analysis_with_http_info(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: DefinitionsmodelAnalysis
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_model_analysis" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter '' is set
        if ('' not in params or
                params[''] is None):
            raise ValueError("Missing the required parameter `` when calling `get_model_analysis`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if '' in params:
            path_params['modelId'] = params['']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['master_token']  # noqa: E501

        return self.api_client.call_api(
            '/models/{modelId}/analysis', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='DefinitionsmodelAnalysis',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_model_by_id(self, , **kwargs):  # noqa: E501
        """Find model by ID  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_model_by_id(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: Definitionsmodel
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_model_by_id_with_http_info(, **kwargs)  # noqa: E501
        else:
            (data) = self.get_model_by_id_with_http_info(, **kwargs)  # noqa: E501
            return data

    def get_model_by_id_with_http_info(self, , **kwargs):  # noqa: E501
        """Find model by ID  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_model_by_id_with_http_info(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: Definitionsmodel
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_model_by_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter '' is set
        if ('' not in params or
                params[''] is None):
            raise ValueError("Missing the required parameter `` when calling `get_model_by_id`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if '' in params:
            path_params['modelId'] = params['']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['master_token']  # noqa: E501

        return self.api_client.call_api(
            '/models/{modelId}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='Definitionsmodel',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_model_fe(self, , **kwargs):  # noqa: E501
        """List model featureImportances  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_model_fe(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_model_fe_with_http_info(, **kwargs)  # noqa: E501
        else:
            (data) = self.get_model_fe_with_http_info(, **kwargs)  # noqa: E501
            return data

    def get_model_fe_with_http_info(self, , **kwargs):  # noqa: E501
        """List model featureImportances  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_model_fe_with_http_info(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_model_fe" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter '' is set
        if ('' not in params or
                params[''] is None):
            raise ValueError("Missing the required parameter `` when calling `get_model_fe`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if '' in params:
            path_params['modelId'] = params['']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # Authentication setting
        auth_settings = ['master_token']  # noqa: E501

        return self.api_client.call_api(
            '/models/{modelId}/feature-importances', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_model_fei(self, , **kwargs):  # noqa: E501
        """List model feature engineering importances  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_model_fei(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_model_fei_with_http_info(, **kwargs)  # noqa: E501
        else:
            (data) = self.get_model_fei_with_http_info(, **kwargs)  # noqa: E501
            return data

    def get_model_fei_with_http_info(self, , **kwargs):  # noqa: E501
        """List model feature engineering importances  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_model_fei_with_http_info(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_model_fei" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter '' is set
        if ('' not in params or
                params[''] is None):
            raise ValueError("Missing the required parameter `` when calling `get_model_fei`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if '' in params:
            path_params['modelId'] = params['']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # Authentication setting
        auth_settings = ['master_token']  # noqa: E501

        return self.api_client.call_api(
            '/models/{modelId}/features-engineering-importances', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_modeldyanmic(self, , **kwargs):  # noqa: E501
        """get model dynamic analysis  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_modeldyanmic(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: DefinitionsmodelDynamicAnalysis
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_modeldyanmic_with_http_info(, **kwargs)  # noqa: E501
        else:
            (data) = self.get_modeldyanmic_with_http_info(, **kwargs)  # noqa: E501
            return data

    def get_modeldyanmic_with_http_info(self, , **kwargs):  # noqa: E501
        """get model dynamic analysis  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_modeldyanmic_with_http_info(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: DefinitionsmodelDynamicAnalysis
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_modeldyanmic" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter '' is set
        if ('' not in params or
                params[''] is None):
            raise ValueError("Missing the required parameter `` when calling `get_modeldyanmic`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if '' in params:
            path_params['modelId'] = params['']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['master_token']  # noqa: E501

        return self.api_client.call_api(
            '/models/{modelId}/dynamic-analysis', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='DefinitionsmodelDynamicAnalysis',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_modelgain(self, , **kwargs):  # noqa: E501
        """get model gain  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_modelgain(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: DefinitionsmodelGain
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_modelgain_with_http_info(, **kwargs)  # noqa: E501
        else:
            (data) = self.get_modelgain_with_http_info(, **kwargs)  # noqa: E501
            return data

    def get_modelgain_with_http_info(self, , **kwargs):  # noqa: E501
        """get model gain  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_modelgain_with_http_info(, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param  : ID of model to return (required)
        :return: DefinitionsmodelGain
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_modelgain" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter '' is set
        if ('' not in params or
                params[''] is None):
            raise ValueError("Missing the required parameter `` when calling `get_modelgain`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if '' in params:
            path_params['modelId'] = params['']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['master_token']  # noqa: E501

        return self.api_client.call_api(
            '/models/{modelId}/gain', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='DefinitionsmodelGain',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
